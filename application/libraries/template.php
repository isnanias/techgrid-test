<?php

class template 
{
	function __construct()
	{
		$this->ci = &get_instance();
	}

	function view($file, $data = null) 
	{
		$data['content'] = $this->ci->load->view($file, $data, true);

		$this->ci->load->view('/layout/template', $data);
	}

	function admin($file, $data = null) 
	{
		$data['content'] = $this->ci->load->view($file, $data, true);

		$this->ci->load->view('/layout/template_admin', $data);
	}
}