<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {
	function __construct()
	{
		parent::__construct();

		$this->load->library('template');

		$this->load->model('mod_login');
		$this->load->model('mod_peserta');

		$this->mod_login->auth();
	}

	function index()
	{
		$this->template->admin('/admin/home');
	}

	function peserta()
	{
		$data['_list'] 	= $this->mod_peserta->list_data();

		$this->template->admin('/admin/peserta', $data);
	}

	function form($no = null)
	{
		$this->no 			= $no;

		$data['_dta'] 		= $this->mod_peserta->detail();

		if($this->input->post('no_peserta') != '' && $this->input->post('no_peserta') != $no)
			$this->form_validation->set_rules('no_peserta', 'No Peserta', 'required|is_unique[peserta.no_peserta]');
		else
			$this->form_validation->set_rules('no_peserta', 'No Peserta', 'required');

		$this->form_validation->set_rules('nama', 'Nama Peserta', 'required');
		$this->form_validation->set_rules('institusi', 'Institusi', 'required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		$this->form_validation->set_message('required', 'Harus diisi');
		$this->form_validation->set_message('is_unique', 'Sudah ada');
		$this->form_validation->set_message('valid_email', 'Tidak valid');
		$this->form_validation->set_error_delimiters('<i style="color:#ce3333;font-size:11px">* ', '</i><br/>');

		if($this->form_validation->run())
		{
			if(empty($data['_dta']))
				$this->mod_peserta->insert_data();
			else
				$this->mod_peserta->update_data($no);

			redirect(base_url('admin/peserta'));
		}

		$this->template->admin('/admin/form', $data);
	}

	function delete($no = null)
	{
		$this->mod_peserta->delete_data($no);

		redirect(base_url('admin/peserta'));
	}
}