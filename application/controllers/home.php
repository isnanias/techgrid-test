<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->library('template');
		$this->load->model('mod_login');
		$this->load->model('mod_peserta');

		$this->mod_login->auth(false);
	}

	public function index()
	{
		$data['msg'] = '';

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_message('required', 'Harus diisi');
		$this->form_validation->set_error_delimiters('<i style="color:#f08383;font-size:11px">* ', '</i>');

		if($this->form_validation->run())
		{	
			if($this->mod_login->cek_login())				
				redirect(base_url('admin'));
			else
				$data['_msg'] 	= '<i style="color:#f08383;font-size:11px">Username atau Password anda salah</i>';
		}

		$data['_list'] 	= $this->mod_peserta->list_data();

		$this->template->view('home', $data);
	}

	function logout()
	{
		$unset = $this->session->unset_userdata(array('username', 'password'));
		
		$this->session->sess_destroy();
			redirect(base_url());
	}
}
