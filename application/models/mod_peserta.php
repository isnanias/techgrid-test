<?php

class mod_peserta extends CI_Model
{
	function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	function list_data()
	{
		return $this->db->order_by('no_peserta')->get('peserta')->result();
	}

	function detail()
	{
		return $this->db->where('no_peserta', $this->no)->get('peserta')->row();
	}

	function insert_data()
	{
		$post = $this->input->post();

		$data = array(
			'no_peserta' 	=> $post['no_peserta'],
			'nama' 			=> $post['nama'],
			'institusi' 	=> $post['institusi'],
			'telepon' 		=> $post['telepon'],
			'email' 		=> $post['email'],
		);

		$this->db->insert('peserta', $data);
	}

	function update_data($no)
	{
		$post = $this->input->post();

		$data = array(
			'no_peserta' 	=> $post['no_peserta'],
			'nama' 			=> $post['nama'],
			'institusi' 	=> $post['institusi'],
			'telepon' 		=> $post['telepon'],
			'email' 		=> $post['email'],
		);

		$this->db->where('no_peserta', $no)->update('peserta', $data);
	}

	function delete_data($no)
	{
		$this->db->where('no_peserta', $no)->delete('peserta');
	}
}