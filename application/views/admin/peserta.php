<button class="btn btn-sm btn-primary" onclick="location.href='<?= base_url() ?>admin/form'">
	<i class="glyphicon glyphicon-plus-sign"></i> Tambah Data
</button>

<table class="table table-striped" width="100%" style="margin-top:10px">
	<thead style="background: #3e6cab;color: #fff">
		<tr>
			<th width="30">No</th>
			<th>No Peserta</th>
			<th>Nama</th>
			<th>Institusi</th>
			<th>Telepon</th>
			<th>Email</th>
			<th width="50">Edit</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach($_list as $val) : ?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $val->no_peserta ?></td>
				<td><?= $val->nama ?></td>
				<td><?= $val->institusi ?></td>
				<td><?= $val->telepon ?></td>
				<td><?= $val->email ?></td>
				<td align="center">
					<a href="<?= base_url() ?>admin/form/<?= $val->no_peserta ?>">
						<i class="glyphicon glyphicon-pencil"></i>
					</a>
					<a href="javascript:void(0)" onclick="if(confirm('Anda yakin akan manghapus data ini?')){location.href='<?= base_url() ?>admin/delete/<?= $val->no_peserta ?>'}">
						<i class="glyphicon glyphicon-trash"></i>
					</a>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>