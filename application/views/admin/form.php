<h4><?= (empty($_dta) ? 'Tambah' : 'Edit') ?> Data Peserta</h4>

<?= form_open('', 'id="form_peserta"') ?>

<table border="0" width="50%">
	<tr valign="top">
		<td width="200">No Peserta</td>
		<td width="10">:</td>
		<td>
			<?= form_input('no_peserta', (empty($_dta) ? '' : $_dta->no_peserta), 'class="form-control input-sm" placeholder="No Peserta"') ?>
			<?= form_error('no_peserta') ?>
		</td>
	</tr>
	<tr valign="top">
		<td>Nama Lengkap</td>
		<td>:</td>
		<td>
			<?= form_input('nama', (empty($_dta) ? '' : $_dta->nama), 'class="form-control input-sm" placeholder="Nama Lengkap"') ?>
			<?= form_error('nama') ?>
		</td>
	</tr>
	<tr valign="top">
		<td>Institusi</td>
		<td>:</td>
		<td>
			<?= form_input('institusi', (empty($_dta) ? '' : $_dta->institusi), 'class="form-control input-sm" placeholder="Institusi"') ?>
			<?= form_error('institusi') ?>
		</td>
	</tr>
	<tr valign="top">
		<td>Telepon</td>
		<td>:</td>
		<td>
			<?= form_input('telepon', (empty($_dta) ? '' : $_dta->telepon), 'class="form-control input-sm" placeholder="Telepon"') ?>
			<?= form_error('telepon') ?>
		</td>
	</tr>
	<tr valign="top">
		<td>Email</td>
		<td>:</td>
		<td>
			<?= form_input('email', (empty($_dta) ? '' : $_dta->email), 'class="form-control input-sm" placeholder="Email"') ?>
			<?= form_error('email') ?>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="right">
			<button type="button" class="btn btn-sm btn-info" onclick="if(confirm('Anda yakin akan membatalkan proses ini?')){location.href='<?= base_url() ?>admin/peserta'}">
				Batal
			</button>
			<button type="button" class="btn btn-sm btn-primary" onclick="if(confirm('Anda yakin akan menyimpan data ini?')){$('#form_peserta').submit()}">
				Simpan
			</button>
		</td>
	</tr>
</table>

<?= form_close() ?>