<table class="table table-striped" width="100%">
	<thead style="background: #3e6cab;color: #fff">
		<tr>
			<th width="30">No</th>
			<th>No Peserta</th>
			<th>Nama</th>
			<th>Institusi</th>
			<th>Telepon</th>
			<th>Email</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach($_list as $val) : ?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $val->no_peserta ?></td>
				<td><?= $val->nama ?></td>
				<td><?= $val->institusi ?></td>
				<td><?= $val->telepon ?></td>
				<td><?= $val->email ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>