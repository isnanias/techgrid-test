<!DOCTYPE html>
<html>
<head>
	<title>Halaman Admin</title>

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/bs/css/bootstrap.css">

	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/bs/js/bootstrap.js"></script>
</head>
<body>

<div class="wrapp">
	<div class="header">
		HALAMAN ADMIN
	</div>
	<div class="menu_atas">
		<ul>
			<li><a href="<?= base_url() ?>admin"><i class="glyphicon glyphicon-home"></i> Home</a></li>
			<li><a href="<?= base_url() ?>admin/peserta"><i class="glyphicon glyphicon-hdd"></i> Data Peserta</a></li>
			<li><a href="javascript:void(0)" onclick="if(confirm('Anda yakin akan keluar?')){location.href='<?= base_url() ?>home/logout'}"><i class="glyphicon glyphicon-user"></i> Logout</a></li>
		</ul>
	</div>
	<div class="content-wrapp" style="height: 81%">
		<div class="content" style="width: 100%;float: none"><?= $content ?></div>
	</div>
	<div class="footer">Copyright &copy; 2016 - Isnan Agung Saputro</div>
</div>

</body>
</html>