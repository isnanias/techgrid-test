<!DOCTYPE html>
<html>
<head>
	<title>Data Peserta Seminar</title>

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/bs/css/bootstrap.css">

	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/bs/js/bootstrap.js"></script>
</head>
<body>

<div class="wrapp">
	<div class="header">
		DAFTAR PESERTA SEMINAR
	</div>
	<div class="content-wrapp">
		<div class="content"><?= $content ?></div>
		<div class="login">
			<h3>Form Login</h3>

			<?= form_open() ?>

			<div class="input-group">
				<div class="input-group-addon">
					<i class="glyphicon glyphicon-user"></i>
				</div>
				<?= form_input('username', '', 'class="form-control input-sm" placeholder="Username"') ?>
			</div>
			<?= form_error('username') ?>

			<div class="input-group" style="margin-top: 7px">
				<div class="input-group-addon">
					<i class="glyphicon glyphicon-lock"></i>
				</div>
				<?= form_input(array('name'=>'password', 'type'=>'password', 'class'=>'form-control input-sm', 'placeholder'=>'Password')) ?>
			</div>
			<?= form_error('password') ?>

			<button class="btn btn-sm btn-primary" style="width: 100%;margin-top: 7px">Login</button>

			<?= form_close() ?>

			<?php 
				if(!empty($_msg))
					echo $_msg;
			?>
		</div>
	</div>
	<div class="footer">Copyright &copy; 2016 - Isnan Agung Saputro</div>
</div>

</body>
</html>